"""
Permet de creer toute les dependances des modules utilises par flask
"""
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
import os.path


def mkpath(p):
    return os.path.normpath(os.path.join(os.path.dirname(__file__), p))

APP = Flask(__name__) #Pour declarer une application flask
APP.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
APP.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('../escape.db'))
APP.config['SECRET_KEY'] = "b70f970f-ef0c-4828-86cb-c883c2eee404"

login_manager = LoginManager(APP)
DB = SQLAlchemy(APP)


