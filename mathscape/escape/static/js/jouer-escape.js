// Initialisation des variables pour le canvas
const description_salle = document.getElementById("description_salle");
const canvas_height = window.innerHeight*0.7;
const canvas_width = (document.getElementsByTagName('main')[0].offsetWidth*80)/100;
const renderer = PIXI.autoDetectRenderer(canvas_width, canvas_height, {backgroundColor: 'black'});
description_salle.insertAdjacentElement('beforebegin', renderer.view);

let ecranIntro = new PIXI.Container();
let ecranSalle = new PIXI.Container();
let listeSalles = [];
let listeIndices = new Map();
let ordreSalles = new Map();
let infosSalleCourante;

function chargerSalleIntro(titreEscape, descEscape){
  let titre = new PIXI.Text(titreEscape, {fontFamily: 'Arial', fontSize: 60, fontStyle: 'italic', fontWeight: 'bold', fill: '#ffffff'});
  titre.position.x = canvas_width/2 - titre.width/2;
  titre.position.y = canvas_height/4;
  ecranIntro.addChild(titre);

  let desc = new PIXI.Text(descEscape, {fontFamily: 'Arial', fontSize: 20, fontStyle: 'italic', fontWeight: 'bold', fill: '#ffffff',  wordWrap: 'true', wordWrapWidth: canvas_width/1.2, align: 'center'});
  desc.position.x = canvas_width/2 - desc.width/2;
  desc.position.y = (canvas_height/4)*2;
  ecranIntro.addChild(desc);

  let button = PIXI.Sprite.fromImage('/static/images/boutonJouer.png');
  button.texture.baseTexture.on('loaded', function(){
    button.scale.set(0.2, 0.2);
    button.buttonMode = true;
    button.interactive = true;
    button.on('click', function(){
      for(salle of listeSalles){
        if(salle.get("affichable") == "True"){
          chargerSalleCourante(salle);
        }
      }
    });
    button.position.x = canvas_width/2 - button.width/2;
    button.position.y = (canvas_height/4)*3;
  });

  ecranIntro.addChild(button);
  renderSalleIntro();
}
function renderSalleIntro(){
  requestAnimationFrame(renderSalleIntro);
  renderer.render(ecranIntro);
}
const buttonEnigme = PIXI.Sprite.fromImage('/static/images/boutonEnigme.png');
buttonEnigme.texture.baseTexture.on('loaded', function(){
  buttonEnigme.scale.set(0.08, 0.08);
  buttonEnigme.buttonMode = true;
  buttonEnigme.interactive = true;
  buttonEnigme.on('click', afficherModalEnigme);
  buttonEnigme.position.x = 10;
  buttonEnigme.position.y = 10;
});
function chargerSalleCourante(salle){
  ecranSalle.removeChildren();

  infosSalleCourante = salle;
  if(salle.get("image") != ""){
    background = new PIXI.Sprite(PIXI.Texture.from(salle.get("image")));
  }else{
    background = new PIXI.Sprite();
  }
  background.position.set(0, 0);
  background.height = canvas_height;
  background.width = canvas_width;
  ecranSalle.addChild(background);

  if(salle.get("zones") != []){
    for(dicoZone of salle.get("zones")){
      tracerRectangle(dicoZone);
    }
  }

  if(salle.get("enigme") != null){
    ecranSalle.addChild(buttonEnigme);
  }

  if(salle.get("description") != ""){
    description_salle.style.display = "flex";
    description_salle.children[0].innerText = salle.get("description");
  }else{
    description_salle.style.display = "none";
  }

  renderEcranSalle();
}
function renderEcranSalle(){
  requestAnimationFrame(renderEcranSalle);
  renderer.render(ecranSalle);
}
function afficherSalle(idSalle){
  for(salle of listeSalles){
    if(salle.get("id") == idSalle && salle.get("affichable") == "True"){
      chargerSalleCourante(salle);
    }
  }
}

function tracerRectangle(dicoZone){
  let sp = new PIXI.Sprite();
  sp.x = dicoZone.get("values")[0]*canvas_width;
  sp.y = dicoZone.get("values")[1]*canvas_height;
  sp.width = dicoZone.get("values")[2]*canvas_height;
  sp.height = dicoZone.get("values")[3]*canvas_width;
  sp.on('click', function(){
    clickZone(dicoZone);
  });

  sp.interactive = true;
  ecranSalle.addChild(sp);
}
function clickZone(dicoZone){
  let indice;
  if(dicoZone.get("contient_indice")){
    for(idZone of listeIndices.keys()){
      if(dicoZone.get("id") == idZone){
        indice = listeIndices.get(idZone);
        if(indice[4] == "False"){
          decouvrirIndice(indice);
        }
        break;
      }
    }
  }
}

function decouvrirIndice(indice){
  // Requete BD mise à jour découverte
  listeIndicesSave.push(indice[0]);
  listeIndicesSave.push(indice[5]);

  // Ajout dans l'inventaire
  const ul = document.getElementById("inventaire");
  if(ul.lastElementChild.nodeName == "P"){
    ul.lastElementChild.remove();
  }
  const li = document.createElement("li");
  li.setAttribute("onclick", "afficherIndiceFromId("+indice[0]+");");
  let img;
  if(indice[3] == ""){
    img = document.createElement("ion-icon");
    img.setAttribute("name", "close");
  }else{
    img = document.createElement("img");
    img.setAttribute("src", indice[3]);
    img.setAttribute("alt", "Image de l'indice");
  }
  li.appendChild(img);
  ul.appendChild(li);
  indice[4] = "True";

  afficherIndice(indice);
}
const sprite = new PIXI.Sprite(PIXI.Texture.fromImage("/static/images/livre.png"));
let texteIndice, texteEnigme, imageIndice;
function afficherIndice(indice){
  sprite.removeChildren(); //supprime tous les enfants
  sprite.scale.x = 0;
  sprite.scale.y = 0;
  texteIndice = new PIXI.Text(indice[1], {fontFamily: 'Arial', fontSize: 30, fontStyle: 'italic', fontWeight: 'bold', fill: '#000000', wordWrap: 'true'});
  // texteEnigme = new PIXI.Text("Indice pour la salle "+trouverSalleEnigmeFromIndiceEnigme(indice), {fontFamily: 'Arial', fontSize: 30, fontStyle: 'italic', fontWeight: 'bold', fill: '#000000'});
  imageIndice = new PIXI.Sprite(PIXI.Texture.fromImage(indice[3]));
  ecranSalle.addChild(sprite);
  requestAnimationFrame(animationLivre);
}
function afficherIndiceFromId(idIndice){
  for(indice of listeIndices){
    if(indice[1][0] == idIndice){
      afficherIndice(indice[1]);
    }
  }
}

function fermerLivre(){
  ecranSalle.removeChild(sprite);
  sprite.removeChildren();
  if(infosSalleCourante.get("enigme")){
    ecranSalle.addChild(buttonEnigme);
  }
  ecranSalle.interactive = false;
}
function animationLivre(){
  ecranSalle.removeChild(buttonEnigme);
  sprite.scale.x += 0.03;
  sprite.scale.y += 0.03;
  sprite.x = (canvas_width/2)-sprite.width/2;
  sprite.y = (canvas_height/2)-sprite.height/2;
  renderer.render(ecranSalle);
  if(sprite.height < canvas_height/1.2){
    requestAnimationFrame(animationLivre);
  }else{
    texteIndice.position.x = sprite.width/2 + 30;
    texteIndice.position.y = 20;
    texteIndice.style.wordWrapWidth = (sprite.width/2) - 30;
    sprite.addChild(texteIndice);
    // texteEnigme.position.x = sprite.width/4 - texteEnigme.width/2;
    // texteEnigme.position.y = texteEnigme.height*2;
    // sprite.addChild(texteEnigme);

    let rapport;
    if(imageIndice.width > imageIndice.height){
      rapport = imageIndice.height/imageIndice.width;
    }else{
      rapport = imageIndice.width/imageIndice.height;
    }
    imageIndice.position.x = (sprite.width/4-imageIndice.width/2);
    imageIndice.position.y = (sprite.height/2-imageIndice.height/2);
    imageIndice.buttonMode = true;
    imageIndice.interactive = true;
    imageIndice.on("click", function(){
      window.open(imageIndice._texture.baseTexture.imageUrl, '_blank');
    });
    sprite.addChild(imageIndice);
    ecranSalle.interactive = true;
    ecranSalle.on('click', fermerLivre);
  }
}

function trouverSalleEnigmeFromIndiceEnigme(indice){
  for(salle of listeSalles){
    if(salle.get("enigme")[0] == indice[2]){
      for(tuple of ordreSalles){
        if(tuple[1] == salle.get("id")){
          return tuple[0];
        }
      }
    }
  }
}

const modalEnigme = new PIXI.Sprite();
function afficherModalEnigme(){
  fermerLivre();
  const enigme = infosSalleCourante.get("enigme");
  const modal = document.getElementById("modal").children[0];

  modal.children[0].children[0].innerText = enigme[1];
  modal.style.backgroundImage = "url("+enigme[4]+")";
  if(enigme[5] == "True"){
    modal.children[0].children[1].setAttribute("readonly", "");
    modal.children[0].children[1].value = "";
    modal.children[0].children[1].setAttribute("placeholder", "Bonne réponse: "+enigme[2]);
    if(modal.children[0].children[2]){
      modal.children[0].children[2].remove();
    }
  }else if(modal.children[0].children[2].lastElementChild.nodeName != "BUTTON"){
    let button = document.createElement("button");
    button.setAttribute("couleur", "validation");
    button.setAttribute("onClick", "resoudreEnigme(this);");
    button.innerText = "Résoudre l'énigme";
    modal.children[0].children[2].appendChild(button);
  }

  afficherModal("modal");
}
function resoudreEnigme(button){
  const enigme = infosSalleCourante.get("enigme");
  if(enigme[2] == parseInt(button.parentNode.previousElementSibling.value) || enigme[2] == parseFloat(button.parentNode.previousElementSibling.value)){
    closeModal("modal");

    let ordre;
    for(tuple of ordreSalles){
      if(tuple[1] == infosSalleCourante.get("id")){
        ordre=tuple[0];
      }
    }

    for(tuple of ordreSalles){
      if(tuple[0] == parseInt(ordre)+1){
        for(salle of listeSalles){
          if(salle.get("id") == tuple[1]){
            sauvegarderAvancee("OK", enigme[0], salle.get("id"));
            break;
          }
        }
        break;
      }
    }
    // sauvegarderAvancee("OK", enigme[0], null);
  }else{
    sauvegarderAvancee("NOP", enigme[0]);
  }
}


setTimeout(incChrono, 1000);
const chrono = document.getElementById("clock");
let min = 5;
let sec = 0;
let listeIndicesSave = [];
function incChrono(){
  if(sec == 0){
    sec = 60;
    min -= 1;
  }
  sec -= 1;
  if(sec < 10){
    sec = "0"+sec;
  }

  chrono.textContent = min+":"+sec;

  if(min == 0 && sec == 0){
    sauvegarderAvancee();
  }else{
    setTimeout(incChrono, 1000);
  }
}
function sauvegarderAvancee(rep=null, enigme=null, salle=null){
  const form = document.createElement("form");
  form.setAttribute("action", "");
  form.setAttribute("method", "post");

  const input1 = document.createElement("input");
  input1.setAttribute("type", "hidden");
  input1.setAttribute("name", "indices");
  input1.setAttribute("value", listeIndicesSave);
  form.appendChild(input1);

  const input2 = document.createElement("input");
  input2.setAttribute("type", "hidden");
  input2.setAttribute("name", "salleCourante");
  if(infosSalleCourante){
    input2.setAttribute("value", infosSalleCourante.get("id"));
  }else{
    input2.setAttribute("value", "");
  }
  form.appendChild(input2);

  const input3 = document.createElement("input");
  input3.setAttribute("type", "hidden");
  input3.setAttribute("name", "enigme");
  if(rep && salle){
    input3.setAttribute("value", [rep, enigme, salle]);
  }else if(rep){
    input3.setAttribute("value", [rep, enigme]);
  }else{
    input3.setAttribute("value", "");
  }
  form.appendChild(input3);

  const input4 = document.createElement("input");
  input4.setAttribute("type", "hidden");
  input4.setAttribute("name", "numEscape");
  input4.setAttribute("value", numEscape);
  form.appendChild(input4);

  const input5 = document.createElement("input");
  input5.setAttribute("type", "hidden");
  input5.setAttribute("name", "est_enigme_speciale");
  input5.setAttribute("value", infosSalleCourante.get("est_enigme_speciale"));
  form.appendChild(input5);

  document.body.appendChild(form);
  form.submit();
}
