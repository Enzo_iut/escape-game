window.flow = function(){
  var container = document.getElementById("integration-list");
  var icons = document.querySelectorAll("#integration-list ion-icon");
  var resizeTimer, w, h;

  window.addEventListener('resize', resizeDebounce); //relance l'animation au resize de la fenetre

  function resizeDebounce(){
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(init, 250);
  }

  function init(){
    w = container.offsetWidth;
    h = container.offsetHeight;

    icons.forEach(function(icon, i){
      TweenMax.set(icon, { y: random(50, h - 150), x: random(w, w+50) });
      animate(icon, i);
    });
  }

  // anime les icones à des valeures différentes pour les axes x et y
  function animate(element, i){
     TweenMax.to(element, random(50,70), { x: -1500, ease: Linear.easeNone, repeat: -1, delay: -115 / icons.length * i });
     TweenMax.to(element, random(6,16), { y: '+=50', repeat: -1, yoyo: true, ease: Sine.easeInOut, delay: random(-16, -6) });
  }

  function random(min,max){
    return min + Math.random() * (max-min);
  };

  return{init: init}
}().init();
