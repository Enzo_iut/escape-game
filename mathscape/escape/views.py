"""
Représente les routes du projet donnant accès aux vues corespondantes
"""
import random
import os
import time

import flask
from babel.dates import format_date
from flask_login import login_user, current_user, logout_user, login_required
from flask import request, session, flash, send_from_directory
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed, FileRequired
from flask_uploads import *
from wtforms import *
from _sha256 import sha256
from flask import render_template, redirect, url_for
import xlrd
from werkzeug.utils import secure_filename

from .commands import loadpers_from_exel_file, loadenigmes, exportetu, exportenigmes
from .requests import *
from .models import Personne
from .app import *

# Variables pour l'affichage de messages sans paramètres dans l'url
message = ""
couleur = ""

# ------------------------------------------
# CLASSES NECESSAIRE AU LOGIN

class LoginForm(FlaskForm):
    email = StringField("adresse mail universitaire")
    password = PasswordField("Mot de passe")

    def get_authenticated(self):
        user = load_personne_by_email(self.email.data)
        if user is None:
            return None
        if (user.check_password(self.password.data)):
            return user

# ------------------------------------------
# PARTIE CONNEXION

@APP.route('/connexion', methods=['GET'])
def connexion():
    """
    La page permettant de se connecter
    """
    form = LoginForm()
    return render_template('connexion.html', form=form, wrong=False)


@APP.route("/connexion", methods=['POST'])
def connexion_post():
    f = LoginForm()
    if f.validate_on_submit():
        user = f.get_authenticated()
        if user:
            login_user(user, remember=True)
            if not user.active:
                user.set_change_authentificated()
                print(user, user.active)
                DB.session.add(user)
                DB.session.commit()
            if user.est_prof:
                return redirect(url_for('gestion_escapes'))
            else:
                return redirect(url_for('accueil_etudiant'))
    return render_template("connexion.html", form=f, wrong=True)


@APP.route("/deconnexion")
def deconnexion():
    print(current_user, current_user.active)
    if current_user.active:
        current_user.set_change_authentificated()
        DB.session.add(current_user)
        DB.session.commit()
    logout_user()
    return redirect(url_for('decouvrir'))


# PARTIE COMUNNE
@APP.route('/')
def decouvrir():
    """
    La première page de l'application (avant de se connecter)
    """
    return render_template('decouvrir.html')


# ------------------------------------------
# PARTIE ÉTUDIANTS
@APP.route("/accueil-etudiant", methods=["GET"])
def accueil_etudiant():
    listeEscape = []
    listeEscapeFini = []
    for idGroupe in getGroupeIdFromIdPers(current_user.id):
        listeEscape.append(getEscapeFromGroupeOuverts(idGroupe))
        listeEscapeFini.append(getEscapeFromGroupeFini(idGroupe))

    return render_template("accueil-etudiant.html", listeEscape=listeEscape, listeEscapeFini=listeEscapeFini)


@APP.template_filter()
def affichageDateTextuel(date):
    """
    Retourne la date(value) avec le format = lundi 23 janvier 2020 a 15:20
    """
    format = "EEEE dd MMMM YYYY ' a ' " + str(date.hour) + ':' + str(date.minute)
    return format_date(date, format, locale='fr')


APP.jinja_env.filters['affichageDateTextuel'] = affichageDateTextuel


@APP.route('/jouer', methods=['POST'])
def jouer():
    """
    La page permettant aux étudiants de jouer à l'escape game
    """
    global message, couleur

    instance = getInstanceEscapeFromId(request.form['numEscape'])
    salleAafficher = ""

    if(len(request.form) > 1):
        if(request.form['indices'] != ""):
            liste = request.form['indices'].split(",")
            for i in range(0, len(liste), 2):
                decouvrirIndice(int(liste[i]), int(liste[i+1]))
            if(len(liste)//2 > 1):
                message = str(len(liste)//2) + " indices ont été découverts"
            else:
                message = "Un indice a été découvert. "
            couleur = "vert"
        if(request.form['enigme'] != ""):
            liste = request.form['enigme'].split(",")
            if(liste[0] == "NOP"):
                if(request.form['est_enigme_speciale'] == "0"):
                    retirerPointsEnigme(liste[1])
                else:
                    retirerPointsEnigmeSpeciale(liste[1])
                couleur = "rouge"
                message += "La réponse à l'énigme n'est pas la bonne"
            else:
                if(request.form['est_enigme_speciale'] == "0"):
                    resoudreEnigme(liste[1])
                else:
                    resoudreEnigmeSpeciale(liste[1])

                if(len(liste)>2):
                    permettreAccesSalle(liste[2])

                couleur = "vert"
                message += "La réponse à l'énigme est la bonne 👏"

        if (request.form['salleCourante'] != ""):
            salleAafficher = request.form['salleCourante']

    listeIndicesDansEscape = []
    listeIndicesTrouves = []
    listeAssociationSalles = []

    for salle in instance.salles:
        if(salle.enigme != None and salle.enigmes_speciales == []) or ((salle.enigmes_speciales != [] and salle.enigme != None) and salle.modele_salle.utilise_enigme_spe != 1):
            listeIndicesDansEscape.append(salle.enigme.indices)
        elif((salle.enigmes_speciales != [] and salle.enigme == None) or ((salle.enigmes_speciales != [] and salle.enigme != None) and salle.modele_salle.utilise_enigme_spe == 1)):
            listeIndicesDansEscape.append(salle.enigmes_speciales[0].indices_speciales)

    for indicesDansSalle in listeIndicesDansEscape:
        if(indicesDansSalle):
            for indice in indicesDansSalle:
                if(indice.est_trouve):
                    listeIndicesTrouves.append(indice)

    instance.salles.sort(key=ordreSalle)
    for salle in instance.salles:
        listeAssociationSalles.append(getAssociationGroupeSalleFromIdSalle(salle.id))

    copieMessage = message
    copieCouleur = couleur
    message = ""
    couleur = ""

    return render_template('jouer-escape.html', instance=instance,
                           groupe=getGroupeFromInstanceEscape(request.form['numEscape']),
                           listeIndicesDansEscape=listeIndicesDansEscape, listeIndicesTrouves=listeIndicesTrouves,
                           note=getNoteFromEscape(request.form['numEscape'], True),
                           listeAssociationSalles=listeAssociationSalles, message=copieMessage, couleur=copieCouleur,
                           salleAafficher=salleAafficher)


def ordreSalle(salle):
    """
    Retourne l'ordre de la salle passée en parametre (pour faire un order by juste au dessu)
    """
    return salle.modele_salle.ordre_salle


@APP.route("/gestion-profil-etudiant", methods=["GET", "POST"])
def gestion_profil_etudiant():
    """
    La page de profil de l'étudiant
    """
    global message, couleur

    if (flask.request.method == "GET"):
        aux_message = message
        aux_couleur = couleur
        message = ""
        couleur = ""
        return render_template('gestion-profil-etudiant.html', message=aux_message, couleur=aux_couleur)
    else:
        pwd = request.form["old-password"]
        if (current_user.check_password(pwd)):
            npwd = request.form["new-password"]
            conf = request.form["confirm-password"]
            if (npwd == conf):
                current_user.set_password(npwd)
                change_password(current_user.id, npwd)
                message = "Le mot de passe à été changé avec succès"
                couleur = "vert"
            else:
                message = "Les mots de passes ne correspondent pas..."
                couleur = "rouge"
        else:
            message = "Le mot de passe actuel est incorrect."
            couleur = "rouge"

    return redirect(url_for('gestion_profil_etudiant'))


# ------------------------------------------
# PARTIE PROFESSEURS
# GESTION ETUDIANTS

@APP.route('/etudiants/', methods=["GET", "POST"])
def gestion_etudiants():
    """
    La page de gestion des étudiants
    """
    global message, couleur
    if (flask.request.method == "POST"):
        fichier = flask.request.files['liste_etudiants']
        nom_fichier = fichier.filename
        if nom_fichier[-4:] == '.xls':
            document = xlrd.open_workbook(file_contents=fichier.read())
            loadpers_from_exel_file(document)
            return redirect(url_for('gestion_etudiants'))
    copieMessage = message
    copieCouleur = couleur
    message = ""
    couleur = ""
    return render_template('gestion-etudiants.html', title="Etudiants", etudiants=get_all_etudiants(),
                           message=copieMessage, couleur=copieCouleur)


@APP.route('/etudiants/export/', methods=["POST"])
def export_etudiants():
    xlsfile = exportetu()
    return send_from_directory(directory="static/", filename=xlsfile, as_attachment=True)


@APP.route('/etudiant/supprimer/', methods=["POST"])
def delete_etudiant():
    """
    Supprime un étudiant avec son id
    """
    supprimerEtudiant(flask.request.form["id"])
    global message, couleur
    message = "L'étudiant à bien été supprimé"
    couleur = "vert"
    return redirect(url_for('gestion_etudiants'))


@APP.route('/etudiants/supprimer', methods=["POST"])
def delete_all_etudiants():
    """
    Supprime tous les étudiants et retourne sur la page de gestion
    """
    global message, couleur
    supprimerTousEtudiants()
    couleur = "vert"
    message = "Tous les étudiants ont bien été supprimés"
    return redirect(url_for('gestion_etudiants'))


class PersonneForm(FlaskForm):
    mode = HiddenField('mode')  # creation ou edition
    id = StringField('Numéro étudiant', [validators.InputRequired(message="Ce champs est requis."),
                                         validators.Length(min=1, max=7,
                                                           message="Le numéro étudiant doit avoir 7 caractères."),
                                         validators.Regexp("^[0-9]+$",
                                                           message="Le numéro étudiant doit contenir seulement des chiffres.")])
    prenom = StringField('Prénom', [validators.InputRequired(message="Ce champs est requis."),
                                    validators.Length(min=2, max=100,
                                                      message="Le prénom doit avoir entre 2 et 100 caractères."),
                                    validators.Regexp("^[a-zA-Z\-.'àâæçéèêëîïôœùûüÿÀÂÆÇnÉÈÊËÎÏÔŒÙÛÜŸ ]+$",
                                                      message="Le prénom doit contenir seulement des lettres, des espaces ou des tirets.")])
    nom = StringField('Nom', [validators.InputRequired(message="Ce champs est requis."),
                              validators.Length(min=2, max=100, message="Le nom doit avoir entre 2 et 100 caractères."),
                              validators.Regexp("^[a-zA-Z\-.'àâæçéèêëîïôœùûüÿÀÂÆÇnÉÈÊËÎÏÔŒÙÛÜŸ ]+$",
                                                message="Le nom doit contenir seulement des lettres, des espaces ou des tirets.")])
    email = StringField('Email', [validators.InputRequired(message="Ce champs est requis."),
                                  validators.Length(min=7, max=100,
                                                    message="L'Email doit avoir entre 7 et 255 caractères."),
                                  validators.Regexp("[^ @]+@[^ @]+.[a-z]+[.][a-z]{2,4}",
                                                    message="L'Email doit être de type \"exemple@exemple.com\".")])
    image = FileField('image',
                      validators=[FileAllowed(IMAGES, 'Seul des images (' + ', '.join(IMAGES) + ') sont acceptés')])


@APP.route('/etudiants/nouveau/')
@APP.route('/etudiants/modifier/<int:identifiant>')
def gestion_etudiants_modifier(identifiant=None):
    """
    La page de création et de modification d'un étudiant
    """
    # Modification
    global message, couleur
    copieMessage = message
    copieCouleur = couleur
    message = ""
    couleur = ""
    if identifiant is not None:
        etu = getPersonneFromId(identifiant)
        mode = "edition"
        prenom = etu.prenom
        nom = etu.nom
        email = etu.email
    # Création
    else:
        mode = "creation"
        etu, prenom, nom, email = None, None, None, None
    f = PersonneForm(mode=mode, image=None, id=identifiant, prenom=prenom, nom=nom, email=email)
    return render_template('modifier-etudiant.html', etudiant=etu, form=f, message=copieMessage, couleur=copieCouleur)


@APP.route('/etudiant/reset_mdp/<int:id>', methods=["POST"])
def gestion_etudiant_reset_mdp(id):
    global message, couleur

    change_password(id=id, password=id)
    couleur = 'vert'
    message = "Le mot de passe a été réinitialisé"
    return redirect(url_for("gestion_etudiants_modifier", identifiant=id))


@APP.route('/etudiant/save', methods=["POST"])
def gestion_etudiants_save():
    """
    Sauvegarde de la création ou l'édition d'un étudiant
    """
    global message, couleur
    f = PersonneForm()
    mode = f.mode.data
    try:
        id = int(f.id.data)
    except ValueError:
        id = None
    etu = getPersonneFromId(id)
    # Vérification de l'id (uniquement en création)
    if mode == "creation" and etu is not None:
        # L'id est déja utilisé donc on renvoie sur la page d'édition avec un message d'erreur
        f.validate_on_submit()
        f.id.errors.append('Ce numéro est déjà utilisé.')
        return render_template("modifier-etudiant.html", etudiant=etu, form=f)
    # Vérification de l'email
    etu_from_email = getPersonneFromEmail(f.email.data)
    if (mode == "edition" and etu_from_email is not None and etu_from_email != etu) or (
            mode == "creation" and etu_from_email is not None):
        # Le mail est déjà utilisé donc on renvoie sur la page d'édition avec un message d'erreur
        f.validate_on_submit()
        f.email.errors.append('Cet Email est déjà utilisé.')
        return render_template("modifier-etudiant.html", etudiant=etu, form=f)

    # On vérifie ensuite le reste des champs du formulaire et on modifie ou crée l'étudiant
    if mode == "edition":
        if f.validate_on_submit():
            if f.image.data is not None and f.image.data.filename != "":
                id_image = stockerImage(f.image.data, PERSONNES_FOLDER)
                etu.id_image_file = id_image
            modifierPersonne(etu, f.nom.data, f.prenom.data, f.email.data, request.form["_avatar"])
            couleur = 'vert'
            message = "Les modifications ont été enregistrées"
            return redirect(url_for('gestion_etudiants'))
    elif mode == "creation" and etu_from_email is None and etu is None:
        if f.validate_on_submit():
            creerEtudiant(identifiant=id, prenom=f.prenom.data, nom=f.nom.data, mail=f.email.data, image=request.form["_avatar"])
            couleur = 'vert'
            message = "Un nouvel étudiant à été créé"
            return redirect(url_for('gestion_etudiants'))
    return render_template("modifier-etudiant.html", etudiant=etu, form=f)


# GESTION ENIGMES SPECIALES
@APP.route('/enigmes', methods=["GET", "POST"])
def gestion_enigmes():
    global message, couleur

    if (flask.request.method == "POST"):
        request = flask.request.form.get('request')
        if request == "import-enigmes":
            fichier = flask.request.files['liste_enigmes']
            nom_fichier = fichier.filename
            if nom_fichier[-4:] == '.xls':
                document = xlrd.open_workbook(file_contents=fichier.read())
                couleur = "vert"
                message = loadenigmes(document)
                return redirect(url_for('gestion_enigmes'))

        elif request == "import-images":
            nbImages = int(flask.request.form["nbImages"])
            images = []
            for i in range(nbImages):
                url = flask.request.form["hiddenImages" + str(i)]
                if (not imageExiste(url)):
                    images.append(url)
            importerImagesEnigmesSpeciales(images)


        elif request == "delete_all":
            supprimerToutesEnigmeSpeciale()
            couleur = "vert"
            message = "Toutes les énigmes ont bien été supprimées"

    copieMessage = message
    copieCouleur = couleur
    message = ""
    couleur = ""

    images = getImagePourEnigmeSpeciale()
    enigmes = getAllEnigmeSpeciale()
    themes = getListeThemes()
    return render_template("gestion-enigmes.html", images=images, enigmes=enigmes, themes=themes, message=copieMessage, couleur=copieCouleur)


@APP.route('/enigmes/indice/image', methods=["POST"])
def gestion_enigme_associer_indice_image():
    print(2)
    id_indice = flask.request.form["indice"]
    id_image = flask.request.form["image-indice"]
    associerIndiceSpecialeImage(id_indice, id_image)
    global message, couleur
    message="L'image a bien été modifiée"
    couleur="vert"
    return redirect(url_for('gestion_enigmes'))


@APP.route('/enigmes/enigme/image', methods=["POST"])
def gestion_enigme_associer_enigme_image():
    print(1)
    id_enigme = flask.request.form["enigme"]
    id_image = flask.request.form["image-enigme"]
    associerEnigmeSpecialeImage(id_enigme, id_image)
    global message, couleur
    message="L'image a bien été modifiée"
    couleur="vert"
    return redirect(url_for('gestion_enigmes'))


@APP.route('/enigmes/delete/images/', methods=["POST"])
def gestion_enigme_delete_all_images():
    images = getImagePourEnigmeSpeciale()
    delete_images(images)
    global message, couleur
    message="Les images ont bien été supprimées"
    couleur="vert"
    return redirect(url_for('gestion_enigmes'))


@APP.route('/enigmes/delete/image', methods=["POST"])
def gestion_enigme_delete_image():
    id_image = flask.request.form["image-id"]
    delete_image(id_image)
    global message, couleur
    message="L'image a bien été supprimée"
    couleur="vert"
    return redirect(url_for('gestion_enigmes'))

@APP.route('/enigmes/export/', methods=["POST"])
def export_enigmes():
    xlsfile = exportenigmes()
    return send_from_directory(directory="static/", filename=xlsfile, as_attachment=True)


@APP.route('/theme/nouveau', methods=["POST"])
def ajout_theme():
    global message, couleur

    nouveauTheme(request.form['nom'])
    couleur = "vert"
    message = "Le thème à bien été créé"
    return redirect(url_for('gestion_enigmes'))


@APP.route('/theme/supprimer/<int:idTheme>')
def suppression_theme(idTheme):
    global message, couleur

    supprimerTheme(idTheme)
    couleur = "vert"
    message = "Le thème à bien été supprimé"
    return redirect(url_for('gestion_enigmes'))


@APP.route('/enigmes/supprimer/', methods=["POST"])
def supprimer_enigme_speciale():
    global message, couleur

    supprimerEnigmeSpeciale(request.form['id'])
    couleur = "vert"
    message = "L'énigme à bien été supprimée"

    return redirect(url_for('gestion_enigmes'))


# GESTION GROUPES
@APP.route('/groupes')
def gestion_groupes():
    """
    La page de gestion des groupes
    """
    global message, couleur

    if message != "":
        copieMessage = message
        copieCouleur = couleur
        message = ""
        couleur = ""
        return render_template('gestion-groupes.html', listeGroupes=getListeGroupesAvecEleves(), message=copieMessage,
                               couleur=copieCouleur)
    return render_template('gestion-groupes.html', listeGroupes=getListeGroupesAvecEleves())


@APP.route('/groupes/nouveau', methods=['GET', 'POST'])
def gestion_groupes_nouveau():
    """
    La page de création d'un groupe
    """
    if (flask.request.method == "POST"):
        res = []
        liste = request.form['personnes'].split(",")
        if (liste != ['']):
            for i in range(0, len(liste), 4):
                res.append(liste[i])
        nouveauGroupe(res, request.form["nom"])
        return redirect(url_for('gestion_groupes'))
    return render_template('nouveau-groupe.html', listeElevePromo=get_all_etudiants(),
                           listeEleveDansAucunGroupe=getListeElevesDansAucunGroupe())


@APP.route('/groupes/supprimer/<int:numGroupe>')
def gestion_groupes_supprimer(numGroupe):
    """
    La route de suppression d'un groupe
    """
    global message, couleur

    groupe = getGroupeFromId(numGroupe)
    for modeleEscape in getListeEscape():
        if groupe in getListeGroupesInEscape(modeleEscape.id):
            couleur = "rouge"
            message = "Le groupe participe à un escape game, il ne peut pas être supprimé"
            return redirect(url_for('gestion_groupes'))
    supprimerGroupe(numGroupe)
    couleur = "vert"
    message = "Le groupe à bien été supprimé"
    return redirect(url_for('gestion_groupes'))


@APP.route('/groupes/supprimer/tout')
def gestion_groupes_supprimer_tout():
    """
    La route de suppression de tous les groupes
    """
    global message, couleur

    cpt = 0
    for groupe in getListeGroupes():
        for modeleEscape in getListeEscape():
            if groupe in getListeGroupesInEscape(modeleEscape.id):
                cpt += 1
            else:
                supprimerGroupe(groupe.id)
    if cpt != 0:
        couleur = "rouge"
        if cpt == 1:
            message = "1 groupe n'a pas été supprimé car il participe à un escape game"
        else:
            message = str(cpt) + " groupes n'ont pas été supprimés car ils participent à un escape game"
    else:
        couleur = "vert"
        message = "Tous les groupes ont bien été suprimés"
    return redirect(url_for('gestion_groupes'))


@APP.route('/groupes/modifier/<int:identifiant>', methods=['GET', 'POST'])
def gestion_groupes_modifier(identifiant):
    """
    La page de modification d'un groupe
    """
    grp = getGroupeFromId(identifiant)
    if (flask.request.method == "POST"):
        res = []
        liste = request.form['personnes'].split(",")
        if (liste != ['']):
            for i in range(0, len(liste), 4):
                res.append(liste[i])
        modifierGroupe(grp, res, request.form["nom"])
    return render_template('modifier-groupe.html', groupe=grp, listeEleveGroupe=getMembresGroupeFromId(identifiant),
                           listeElevePromo=get_all_etudiants(),
                           listeEleveDansAucunGroupe=getListeElevesDansAucunGroupe())


# GESTION ESCAPE
@APP.route('/escapes/')
def gestion_escapes():
    """
    La page de gestion des escapes games
    """
    global message, couleur

    if message != "":
        copieMessage = message
        copieCouleur = couleur
        message = ""
        couleur = ""
        return render_template('gestion-escapes.html', message=copieMessage, couleur=copieCouleur,
                               listeEscapes=getListeEscapeParProf(current_user.get_id()))
    return render_template('gestion-escapes.html', listeEscapes=getListeEscapeParProf(current_user.get_id()))


@APP.route('/escapes/nouveau', methods=['POST'])
def gestion_escapes_nouveau():
    """
    La route de création d'un escape game
    """
    global message, couleur
    nouveauModeleEscape(request.form['nom'], request.form['description'], current_user.get_id())
    message = "L'escape game à bien été créé"
    couleur = "vert"
    return redirect(url_for('gestion_escapes'))


@APP.route('/escapes/supprimer/<int:idEscape>')
def gestion_escapes_supprimer(idEscape):
    """
    La route de suppresion d'un escape game
    """
    global message, couleur
    supprimerModeleEscape(idEscape)
    message = "L'escape game à bien été supprimé"
    couleur = "vert"
    return redirect(url_for('gestion_escapes'))


@APP.route('/escapes/supprimer/tout')
def gestion_escapes_supprimer_tout():
    """
    La route de suppresion de tous les escapes games d'une personne
    """
    global message, couleur
    for modeleEscape in getListeEscapeParProf(current_user.get_id()):
        supprimerModeleEscape(modeleEscape.id)
    message = "Les escapes games que vous avez créé ont bien tous été supprimés"
    couleur = "vert"
    return redirect(url_for('gestion_escapes'))


@APP.route('/escapes/modifier/<int:idEscape>/versions/supprimer/<int:identifiant>')
def supprimer_version(idEscape, identifiant):
    """
    Route utilisée pour supprimer une version (instance) d'escape game (modèle)
    """
    supprimerInstanceEscapeFromId(identifiant)
    global couleur, message
    message = "La version à bien été supprimée"
    couleur = "vert"
    return redirect('/escapes/modifier/' + str(idEscape))


@APP.route('/escapes/modifier/<int:idEscape>/versions/ajouter/', methods=['POST'])
def ajouter_version(idEscape):
    """
    Route utilisée pour ajouter une version (instance) d'escape game (modèle)
    """
    global message, couleur
    if (getListeSallesFromEscapeId(idEscape) != []):
        for (cle, idGroupe) in request.form.to_dict().items():
            ajouterVersionPourEscape(idEscape, idGroupe)
        message = "La version à bien été créée"
        couleur = "vert"
    else:
        message = "Vous devez ajouter au moins une salle au scénario pour créer une version de cet escape game"
        couleur = "rouge"
    return redirect('/escapes/modifier/' + str(idEscape))


@APP.route('/escapes/modifier/<int:identifiant>', methods=['GET', 'POST'])
def gestion_escapes_modifier(identifiant):
    """
    La page de modification d'un escape game
    """
    global message, couleur

    esc = getModeleEscapeFromId(identifiant)
    if (flask.request.method == "POST"):  # Modification dans la BD
        nom = request.form['nom']
        description = request.form['description']
        modifierModeleEscape(esc, nom, description)
        for (cle, valeur) in request.form.to_dict().items():
            if (cle[:4] == 'date'):
                if (cle[:9] == 'dateDebut'):
                    modifierDateDebutInstanceEscape(cle[10:], valeur)
                else:
                    modifierDateFinInstanceEscape(cle[8:], valeur)
        couleur = "vert"
        message = "Les informations ont bien été modifiées"
        return redirect('/escapes/modifier/' + str(identifiant))
    if message != "":
        copieMessage = message
        copieCouleur = couleur
        message = ""
        couleur = ""
        return render_template('modifier-escape.html', escape=esc, message=copieMessage, couleur=copieCouleur,
                               listeVersions=getListeInstancesAvecGroupesFromEscapeId(identifiant),
                               listeSalles=getListeSallesFromEscapeId(identifiant),
                               listeGroupesAjoutables=getListeGroupesAjoutableModeleEscapeFromId(identifiant))
    return render_template('modifier-escape.html', escape=esc,
                           listeVersions=getListeInstancesAvecGroupesFromEscapeId(identifiant),
                           listeSalles=getListeSallesFromEscapeId(identifiant),
                           listeGroupesAjoutables=getListeGroupesAjoutableModeleEscapeFromId(identifiant))


# GESTION PROFIL
@APP.route('/profil', methods=['GET', 'POST'])
def gestion_profil():
    """
    La page de modification et d'affichage des informations du profil
    """
    if (request.method == "POST"):
        modifierPersonne(current_user, request.form['nom'], request.form['prenom'], request.form['mail'],
                         request.form["_avatar"])
        return render_template('gestion-profil.html', personne=current_user, message="Le profil à bien été mis à jour",
                               couleur="vert")
    return render_template('gestion-profil.html', personne=current_user)


# GESTION SCENARIO
@APP.route('/scenario/modifier/<int:identifiant>/salle/<int:idSalle>/')
def gestion_scenario_modifier_affichage_salle_courante(identifiant, idSalle):
    """
    La page de modification du scénario d'un escape game avec l'affichage d'une salle en particulier
    """
    global message, couleur

    escape = getModeleEscapeFromId(identifiant)
    listeSalles = getListeSallesFromEscapeId(identifiant)
    salleCourante = getModeleSalleFromId(idSalle)
    listeZones = getListeZonesFromModeleSalle(idSalle)
    listeIndices = getListeModeleIndiceFromSalle(idSalle)
    enigmeSalle = getEnigmeSalleFromModeleSalleId(idSalle)
    imageFond = getImageFromId(salleCourante.id_image)
    listeModeleEnigmes = getListeModeleEnigmeFromModeleEscape(identifiant)
    listeTheme = getListeThemes()
    enigmesSpe = getAllEnigmeSpeciale()
    ListeGroupe = getListeGroupes()
    listeVersions = getListeGroupesInEscape(identifiant)

    print(salleCourante.salles)
    if message != "":
        copieMessage = message
        copieCouleur = couleur
        message = ""
        couleur = ""
        return render_template('modifier-scenario.html', message=copieMessage, couleur=copieCouleur, escape=escape,
                               listeSalles=listeSalles, salleCourante=salleCourante, listeZones=listeZones,
                               listeVersions=listeVersions,
                               listeIndices=listeIndices, enigmeSalle=enigmeSalle, imageFond=imageFond,
                               ListeGroupe=ListeGroupe,
                               listeModeleEnigmes=listeModeleEnigmes, listeTheme=listeTheme, enigmesSpe=enigmesSpe)
    return render_template('modifier-scenario.html', escape=escape, listeSalles=listeSalles,
                           salleCourante=salleCourante, listeVersions=listeVersions,
                           listeZones=listeZones, listeIndices=listeIndices, enigmeSalle=enigmeSalle,
                           imageFond=imageFond,
                           listeModeleEnigmes=listeModeleEnigmes, listeTheme=listeTheme, enigmesSpe=enigmesSpe,
                           ListeGroupe=ListeGroupe)


@APP.route('/scenario/modifier/<int:identifiant>/salle/modifier/<int:idSalle>', methods=['POST'])
def gestion_scenario_modifier_salle(identifiant, idSalle):
    """
    La route de sauvegarde des changements effectués sur un modèle de salle d'un escape game
    """
    global message, couleur

    message = "Les données ont été enregistré"
    couleur = "vert"
    modifierModeleSalle(int(idSalle), request.form['descriptionSalle'], int(request.form['estLibre']),
                        int(request.form['estPersonalisee']), request.form['hiddenImageFond'])
    supprimerZonesSalle(idSalle)
    if (request.form['listeZones'] != ""):
        res = []
        liste = request.form['listeZones'].split(',')
        for i in range(0, len(liste), 4):
            res.append((liste[i], liste[i + 1], liste[i + 2], liste[i + 3]))
        associerZonesModeleSalle(int(idSalle), res)

    return redirect('/scenario/modifier/' + str(identifiant) + '/salle/' + str(idSalle))


@APP.route('/scenario/modifier/<int:identifiant>/salle/supprimer/<int:idSalle>')
def supprimer_salle(identifiant, idSalle):
    """
    La route de suppression d'un modèle de salle
    """
    global message, couleur
    couleur = "vert"
    message = supprimerModeleSalle(idSalle, identifiant)
    listeModeleSalles = getListeSallesFromEscapeId(identifiant)
    if (listeModeleSalles):
        return redirect('/scenario/modifier/' + str(identifiant) + '/salle/' + str(listeModeleSalles[0].id))
    return redirect('/escapes/modifier/' + str(identifiant))


@APP.route('/scenario/modifier/<int:identifiant>/salle/<int:idSalle>/modifier_mode_utilisation')
def modifier_mode_utilisation(identifiant, idSalle):
    global message, couleur
    modifierModeUtilisation(idSalle)
    message = "Le mode à bien été changé"
    couleur = "vert"
    return redirect('/scenario/modifier/' + str(identifiant) + "/salle/" + str(idSalle))


@APP.route('/scenario/modifier/<int:identifiant>/salle/<int:idSalle>/indices/enregistrer', methods=["POST"])
def enregistrer_indices(identifiant, idSalle):
    """
    La route de d'enregistrement des indices du scénario
    """
    global message, couleur
    liste = request.form['listeIndices'].split(",")
    for i in range(0, len(liste), 4):
        liste[i + 1] = liste[i + 1].replace("&1234&", ",")
        liste[i + 3] = liste[i + 3].replace("&1234&", ",")
        if (liste[i][0:3] == "nv-"):
            ajouterIndice(int(idSalle), liste[i + 1], liste[i + 2][1:], liste[i + 3])
        elif (liste[i][0:4] == "del-"):
            supprimerModeleIndice(int(liste[i][4:]))
        else:
            modifierIndice(liste[i], liste[i + 1], liste[i + 2][1:], liste[i + 3])

    associerZonesModeleSalleSansChangerZones(int(idSalle))
    message = "Modifications enregistrées"
    couleur = "vert"
    return redirect('/scenario/modifier/' + str(identifiant) + "/salle/" + str(idSalle))


@APP.route('/scenario/modifier/<int:identifiant>/salle/<int:idSalle>/modifier/enigme/', methods=["POST"])
def modifier_enigme(identifiant, idSalle):
    """
    La route de d'enregistrement de l'énigme
    """
    global message, couleur
    modifierEnigme(int(request.form['idEnigme']), request.form['enigme'], request.form['solutionEnigme'],
                   int(request.form['nbPoints']), request.form['hiddenImageEnigme'])
    message = "Modifications enregistrées"
    couleur = "vert"
    return redirect('/scenario/modifier/' + str(identifiant) + "/salle/" + str(idSalle))


@APP.route('/scenario/modifier/<int:identifiant>/salle/<int:idSalle>/supprimer/enigme/<int:idEnigme>', methods=["GET"])
def supprimer_enigme(identifiant, idSalle, idEnigme):
    """
    La route de de supression d'une enigme
    """
    global message, couleur
    supprimerModeleEnigme(idEnigme)

    message = "Modifications enregistrées"
    couleur = "vert"
    return redirect('/scenario/modifier/' + str(identifiant) + "/salle/" + str(idSalle))


@APP.route('/scenario/modifier/<int:identifiant>/salle/<int:idSalle>/ajouter/enigme', methods=["GET"])
def ajouter_enigme(identifiant, idSalle):
    """
    La route d'ajout d'une énigme pour la salle
    """
    global message, couleur
    ajouterModeleEnigme(idSalle)
    message = "L'énigme à bien été créée"
    couleur = "vert"
    return redirect('/scenario/modifier/' + str(identifiant) + "/salle/" + str(idSalle))


@APP.route('/scenario/modifier/<int:identifiant>/salle/<int:idSalle>/ajouter/groupe', methods=["POST"])
def ajouter_groupe(identifiant, idSalle):
    """
    La route d'ajout d'une énigme speciale pour la salle
    """
    global message, couleur
    idEnigmeSpe = int(request.form['idEnigmeSpeChoisie'])
    idGroupe = int(request.form['idGroupeChoisi'])
    ajouterAssociationEnigmeSpecialSalle(idEnigmeSpe, idSalle)
    ajouterAssociationGroupeEnigmeSpeciale(idEnigmeSpe, idGroupe)
    res = associerIndicesEnigmeSpeciale(idSalle, idEnigmeSpe, idGroupe)
    message = res[1]
    couleur = res[0]
    return redirect('/scenario/modifier/' + str(identifiant) + "/salle/" + str(idSalle))



@APP.route('/scenario/modifier/<int:identifiant>/salle/ajouter/<int:numeroOrdre>')
def ajouter_salle(identifiant, numeroOrdre):
    """
    La route d'ajout d'un modèle de salle à l'escape game'
    """
    idSalle = ajouterModeleSalleEscapeReturnId(identifiant, numeroOrdre)
    return redirect('/scenario/modifier/' + str(identifiant) + '/salle/' + str(idSalle))


# ENIGMES SPECIALES DEPUIS SCENARIO
@APP.route('/scenario/modifier/<int:identifiant>/salle/<int:idSalle>/enigme_speciale/', methods=["POST"])
def enigme_speciale_scenario(identifiant, idSalle):
    """
    La route d'association d'une énigme spéciale depuis le scénario
    """
    global message, couleur
    res = associerEnigmeSpecialeSalle(idSalle, int(request.form['difficulte']), int(request.form['theme']))
    message = res[1]
    couleur = res[0]
    return redirect('/scenario/modifier/' + str(identifiant) + "/salle/" + str(idSalle))


@APP.route('/scenario/modifier/<int:identifiant>/salle/<int:idSalle>/enigmeSpe/supprimer/<int:idEnigmeSpe>')
def supprimer_enigme_speciale_scenario(identifiant, idSalle, idEnigmeSpe):
    """
    La route de suppression de l'association d'une énigme spéciale à un groupe et une salle
    """
    global message, couleur
    supprimerAssociationEnigmeSpeSalleGroupe(idEnigmeSpe)
    message = "L'association à bien été supprimée"
    couleur = "vert"
    return redirect('/scenario/modifier/' + str(identifiant) + "/salle/" + str(idSalle))


@APP.route('/scenario/modifier/<int:identifiant>/salle/<int:idSalle>/enigmeSpe/supprimer/tout')
def supprimer_toutes_enigme_speciale_scenario(identifiant, idSalle):
    """
    La route de suppression des associations d'énigmes spéciales et indices à une salle
    """
    global message, couleur
    modeleSalle = getModeleSalleFromId(idSalle)
    if(modeleSalle.salles):
        for salle in modeleSalle.salles:
            if(salle.enigmes_speciales):
                for enigmeSpe in salle.enigmes_speciales:
                    supprimerAssociationEnigmeSpeSalleGroupe(enigmeSpe.id)
    message = "Toutes les associations ont bien été supprimées"
    couleur = "vert"
    return redirect('/scenario/modifier/'+str(identifiant)+"/salle/"+str(idSalle))

#CONSULTER LES STATISTIQUES
@APP.route('/statistiques')
def statistiquesV1():
    """
    La page affichant les escapes a selectionner pour les statistiques de l'APPlication
    """
    return render_template('statistiquesV1.html', escapes=getListeEscape())


@APP.route('/statistiques/<int:escape>')
def statistiquesV2(escape):
    """
    La page affichant affichant les escapes et les groupes après statistiquesV1 affichant les statistiques de l'APPlication
    """
    return render_template('statistiquesV2.html', escapes=getListeEscape(), groupes=getListeGroupesInEscape(escape),
                           esc=getModeleEscapeFromId(escape))


@APP.route('/statistiques/<int:escape>/<int:groupe>')
def statistiquesV3(escape, groupe):
    """
    La page affichant les statistiques de l'application.
    """
    instanceDuGroupeDeLEscapeGame = getInstanceFromEscapeIdAndGroupeId(escape, groupe)
    return render_template('statistiquesV3.html', escapes=getListeEscape(), groupes=getListeGroupesInEscape(escape),
                           pourcentage=tauxDeProgression(instanceDuGroupeDeLEscapeGame[0].id, groupe),
                           nbPoints=getNbPointsGroupe(instanceDuGroupeDeLEscapeGame[0].id),
                           nbPointsMax=getNbPointsMaxGroupe(instanceDuGroupeDeLEscapeGame[0].id),
                           valeur=getValeur(instanceDuGroupeDeLEscapeGame[0].id), esc=getModeleEscapeFromId(escape),
                           escInstDateDebut=instanceDuGroupeDeLEscapeGame[0].date_debut.__str__()[:10],
                           escInstDateFin=instanceDuGroupeDeLEscapeGame[0].date_fin.__str__()[:10])


# GESTION PROFIL
@APP.route('/escapes/<int:numEscape>/versions/<int:numVersion>')
def gestion_versions_instances(numEscape, numVersion):
    """
    La page affichant les informations d'une instance d'escape game
    """
    return render_template('gestion-version.html', numVersion=numVersion, numEscape=numEscape)


@APP.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404
